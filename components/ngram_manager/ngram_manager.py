import sys
import re
import random
import boto3
import json
import logging

from boto3.dynamodb.conditions import Key, Attr

from tweet_manager.tweet_manager import (
    get_timeline
)

from integration_manager.rest_integration import Response
from integration_manager.s3_integration import locate_s3_bucket
from integration_manager.util import generate_id


logger = logging.getLogger()


def generate_random_tweet(user_handle):

    response = get_timeline(user_handle)
    corpus = "\n".join([tweet.get("text") for tweet in response.get("timeline")])
    username = response.get("username")
    previous_ngrams = _get_ngrams_if_exists(username)
    if (corpus is not None):
        ngrams = tokenize_corpus(corpus, previous_ngrams)
        _persist_ngrams(username, ngrams)
    elif (corpus is None and previous_ngrams is not None):
        ngrams = previous_ngrams
    else:
        return Response(success=False, detail="Failed to generate a sentence")
    return Response(success=True, detail=generate_sentence(ngrams))


def tokenize_corpus(corpus, ngrams = None, n = 3):

    if ngrams is None:
        ngrams = {}

    array_hold = []

    for i in range(0, n):
        array_hold.append("")

    corpus.rstrip()
    corpus = re.sub(r"\b(https\:\/\/t\.co\/[a-zA-Z0-9]*)\b", "", corpus)
    corpus = corpus.replace("“", "", -1)
    corpus = corpus.replace("”", "", -1)
    corpus = corpus.replace("\"", "", -1)
    corpus = corpus.replace(".", " " + _return_n_strings(".", n) + " ", -1)
    corpus = corpus.replace("!", " " + _return_n_strings("!", n) + " ", -1)
    corpus = corpus.replace("?", " " + _return_n_strings("?", n) + " ", -1)

    array = array_hold[::-1]
    array2 = corpus.split()
    array.extend(array2)
    array = _remove_values_from_list(array, '')
    
    array_hold = array
    for i in range(0, len(array) - n):
        j = i + n - 1
        first = ""
        ngram = ""
        if (j > len(array)):
            continue
        for k in range(i, j + 1):

            if ( k < j ):
                first += str(array[k]) + " "
            else:
                ngram += str(array[k]) + " "
        ngram.rstrip()
        if (first in ngrams):
            ngrams[first]["_total"] += 1
        else:
            ngrams[first] = {"_total": 1}
        ngrams[first][ngrams[first]["_total"]] = ngram
    return ngrams


def generate_sentence(ngrams, n = 3):

    sentence = ""
    history = _initialize_history(n)

    while ("." not in sentence and "!" not in sentence and "?" not in sentence):
        prevWords = _get_previous_words(history, n)
        sentenceObject = _determine_next_word(ngrams, prevWords, history)
        history = sentenceObject.get("history")
        sentence += sentenceObject.get("word")

    sentArray = sentence.split()
    if (len(sentArray) < 10):
        return generate_sentence(ngrams, n)
    else:
        sentence = re.sub(r"\s+$", "", sentence)
        sentenceHold = sentence
        sentArray = sentenceHold.split()
        sentence = ""
        sentence = " ".join(sentArray)
        sentence = sentence.replace("amp;", "", -1)
        sentence = sentence.replace(" …", "...", -1)
        sentence = sentence.replace(" .", ".", -1)
        sentence = sentence.replace(" !", "!", -1)
        sentence = sentence.replace(" ?", "?", -1)
        return sentence 


def _determine_next_word(ngrams, prevWords, history):

    count = ngrams[prevWords]['_total']
    intRandom = random.randrange(count)
    word = ngrams[prevWords][intRandom + 1]
    history.append(word)
    return {"history": history, "word": word}


def _get_previous_words(history, n):

    prevWords = "".join(history[-(n - 1):])
    return prevWords


def _initialize_history(n):

    return [punctuation + " " for punctuation in _return_n_strings(". ", n - 1).split()]


def _remove_values_from_list(the_list, val):

   return [value for value in the_list if value != val]


def _return_n_strings(string, n):

    for i in range(0, n):
        string += " " + string

    return string


def _get_ngrams_if_exists(username):

    ngrams_bucket = _get_ngrams_s3_bucket()

    try:
        ngrams = eval(ngrams_bucket.Object("{}.txt".format(username)).get().get("Body").read())
    except Exception as e:
        logger.error(
            ("Unable to retrieve the specified user's timeline request: %s."),
            e
        )
        return None
    
    return ngrams


def _persist_ngrams(username, ngrams):

    ngrams_bucket = _get_ngrams_s3_bucket()
    ngrams_bucket.put_object(
        Key="{}.txt".format(username), 
        Body=str(ngrams)
    )


def _get_ngrams_s3_bucket():

    s3 = boto3.resource("s3")
    bucket_name = locate_s3_bucket("ngrams")
    return s3.Bucket(bucket_name)

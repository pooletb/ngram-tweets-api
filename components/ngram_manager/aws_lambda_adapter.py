import json
import logging
from json import JSONDecodeError

from integration_manager.aws_lambda_integration import encode_response
from integration_manager.rest_integration import Response
from . import ngram_manager

#
#
logger = logging.getLogger()


def generate_random_tweet(event, context):

  request_query = event["queryStringParameters"]
  if "userHandle" in request_query:
    user_handle = request_query["userHandle"]
  else:
    return encode_response(Response(success=False, detail="\"user handle\" query parameter is required."))

  return encode_response(ngram_manager.generate_random_tweet(user_handle))

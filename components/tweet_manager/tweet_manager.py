import time
import logging
import json
import os

import tweepy
import boto3

from integration_manager.rest_integration import Response
from integration_manager.twitter_integration import connect_to_twitter
from integration_manager.s3_integration import locate_s3_bucket
from integration_manager.util import generate_id


logger = logging.getLogger()


def get_timeline(user_handle):

    twitter = connect_to_twitter()
    username = _get_username(twitter, user_handle)

    timeline = _get_timeline_if_exists(username)
    
    def find_breaking_point(new_tweet):
        return [tweet for tweet in timeline if tweet.get("id") == new_tweet.id_str]
        

    new_tweets = []

    try:
        for tweet in tweepy.Cursor(twitter.user_timeline, tweet_mode="extended", screen_name = username).items():
            should_break = find_breaking_point(tweet)
            if (not tweet.retweeted and "RT @" not in tweet.full_text and len(should_break) is 0):
                _append_tweet(new_tweets, tweet._json)
            elif (len(should_break) > 0):
                break
    except Exception as e:
        logger.error(
            ("Failed to persist timeline: %s."),
            e
        )
        if timeline is not None:
            return {"timeline": new_tweets, "username": username}
        else:
            return None

    timeline.extend(new_tweets)
    _persist_timeline(username, timeline)
    return {"timeline": new_tweets, "username": username}


def _get_username(twitter, user_handle):

    if (user_handle[0] == "@"):
        mode_flag = "username"
        user_id = user_handle[1:]
    else:
        mode_flag = "id"
        user_id = user_handle

    try:
        user = twitter.get_user(user_id)
        if (mode_flag == "id"):
            username = user.screen_name
        else:
            username = user_id
    except Exception as e:
        logger.error(
            ("Failed to find a user with the specified user handle: %s."),
            e
        )
        return None

    return username


def _get_timeline_if_exists(username):

    timelines_bucket = _get_timelines_s3_bucket()

    try:
        timeline = eval(timelines_bucket.Object("{}.txt".format(username)).get().get("Body").read())
        print('timeline found')
    except Exception as e:
        logger.error(
            ("Unable to retrieve the specified user's timeline request: %s."),
            e
        )
        return []

    return timeline
    

def _append_tweet(timeline, full_tweet):

    tweet = {
        "text": full_tweet.get("full_text"),
        "id": full_tweet.get("id_str")
    }
    timeline.append(tweet)


def _persist_timeline(username, timeline):

    timelines_bucket = _get_timelines_s3_bucket()
    timelines_bucket.put_object(
        Key="{}.txt".format(username), 
        Body=str(timeline)
    )


def _get_timelines_s3_bucket():

    s3 = boto3.resource("s3")
    bucket_name = locate_s3_bucket("timelines")
    return s3.Bucket(bucket_name)




from .aws_secrets_integration import get_secret
import tweepy


DEFAULT_SECRET_NAME = "twitterConnectionInformation"


class TwitterConnectionDescriptor:
    def __init__(self, wrapped_dict):
        self.wrapped_dict = wrapped_dict
        self.missing_keys = []
        self.consumer_key = self._get_dict_value("consumerKey")
        self.consumer_secret = self._get_dict_value("consumerSecret")
        self.access_token = self._get_dict_value("accessToken")
        self.access_secret = self._get_dict_value("accessSecret")
        if self.missing_keys:
            raise Exception(
                "The twitter oAuth configuration is missing the following keys: {keys}.".format(
                    keys=self.missing_keys
                )
            )

    def _get_dict_value(self, key):

        value = self.wrapped_dict.get(key)
        if value is None:
            self.missing_keys.append(key)
        return value

    def to_oauth_handlerl(self):

        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        auth.set_access_token(self.access_token, self.access_secret)
        return auth


def get_default_oauth_handler():

    connection_information = get_secret(DEFAULT_SECRET_NAME)
    descriptor = TwitterConnectionDescriptor(connection_information)
    return descriptor.to_oauth_handlerl()


def connect_to_twitter():

    auth = get_default_oauth_handler()
    twitter = tweepy.API(auth, wait_on_rate_limit=False)
    return twitter

import re

import jsonpickle

JSON_CONTENT_TYPE = "application/json"


def encode_response(body, status_code=200, content_type=JSON_CONTENT_TYPE):

    if content_type == JSON_CONTENT_TYPE:
        body = jsonpickle.encode(body, unpicklable=False)

    return {
        "isBase64Encoded": False,
        "statusCode": status_code,
        "headers": {"Content-Type": content_type},
        "body": body
    }
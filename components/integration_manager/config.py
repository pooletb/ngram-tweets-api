import os


def get_config_base():

    return os.environ.get("SSM_PARAMETER_BASE_NAME")

import boto3
from .config import get_config_base


BATCH_GET_ITEM_MAX_BATCH_SIZE = 100


def locate_dynamodb_table(logical_table_name):

    aws_systems_manager = boto3.client('ssm')
    config_base = get_config_base()
    table_name_parameter = aws_systems_manager.get_parameter(
        Name="{}/dynamodbTableNames/{}".format(config_base, logical_table_name))
    return table_name_parameter["Parameter"]["Value"]
